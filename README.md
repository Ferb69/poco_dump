## qssi-user 12
12 SKQ1.211019.001 V13.0.2.0.SJGEUXM release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: karna
- Brand: POCO
- Flavor: qssi-user
- Release Version: 12
12
- Id: SKQ1.211019.001
- Incremental: V13.0.2.0.SJGEUXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: POCO/karna_eea/karna:12/RKQ1.211019.001/V13.0.2.0.SJGEUXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.211019.001-V13.0.2.0.SJGEUXM-release-keys
- Repo: poco_karna_dump
